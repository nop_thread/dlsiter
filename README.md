# DLsiter

![Minimum supported rustc version: 1.59](https://img.shields.io/badge/rustc-1.59+-lightgray.svg)

DLsite client for Pro.

## (Sub)commands

### `login`

Logs in to DLsite.

Saves the secret data (session token) to a file.

#### Options

* `--check`
    + Checks the login state, without attempting to login.
* `--save-id-password`
    + Saves the login ID and the password to the credentials files.
      Note that this is **INSECURE** since it saves them as plain text.

### `logout`

Logs out from DLsite.

This invalidates the saved session token, and updates the file for the secret data.

### `fetch`

Fetches data from DLsite.

By default, only new data since the last time will be fetched.

#### Options

* `--refetch-all`
    + Fetches the all data, even if some of them are already fetched.

### `stats`

Prints statistics.

Currently, sum of prices, number of items, and average prices are calculated.

#### Options

* `--since-i` or `--since-x` (mutually exclusive)
    + The start time of the range to get statistics, in JST.
    + `--since-i` is inclusive, `--since-x` is exclusive.
* `--until-i` or `--until-x` (mutually exclusive)
    + The end time of the range to get statistics, in JST.
    + `--until-i` is inclusive, `--until-x` is exclusive.

## Data files

This application saves the data (credentials and fetched DLsite data) to the
platform-specific application data directory.
See <https://docs.rs/directories/4.0.1/directories/struct.ProjectDirs.html#method.data_dir>.

For example, if you are Linux user, the data will be saved under
`$HOME/.local/share/dlsiter` (if you haven't been changed `$XDG_DATA_DIR`
environment variable).

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
