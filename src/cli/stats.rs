//! Show stats.

use std::ops::{Bound, RangeBounds};

use anyhow::{Context as _, Result};
use rusqlite::named_params;
use time::OffsetDateTime;

use crate::cli::StatsOpts as Opts;
use crate::db::DbPool;

/// Runs `stats` subcommand.
pub fn run(pool: &DbPool, opts: &Opts) -> Result<()> {
    let conn = pool
        .connect()
        .context("failed to connect to the database")?;

    let now = OffsetDateTime::now_utc();
    let range = opts.time_range(now).context("failed to get time range")?;
    let time_format =
        time::format_description::parse("[year]-[month]-[day] [hour]:[minute]:[second] [offset_hour sign:mandatory]:[offset_minute]")
            .expect("time format description must be valid");

    {
        print!("#");
        match range.0 {
            Bound::Included(v) => print!(" since {} (inclusive)", v.format(&time_format)?),
            Bound::Excluded(v) => print!(" since {} (exclusive)", v.format(&time_format)?),
            Bound::Unbounded => {}
        }
        match range.1 {
            Bound::Included(v) => print!(" until {} (inclusive)", v.format(&time_format)?),
            Bound::Excluded(v) => print!(" until {} (exclusive)", v.format(&time_format)?),
            Bound::Unbounded => {}
        }
        if range.0 == Bound::Unbounded && range.1 == Bound::Unbounded {
            print!(" all time");
        }
        println!();
    }

    let price = consumed_price(&conn, range).context("failed to get consumed price")?;
    println!("price: {price}");

    let products_all = count_products(&conn, range, false).context("failed to count products")?;
    let products_nonfree =
        count_products(&conn, range, true).context("failed to count non-free products")?;
    println!("the number of products: {products_all}");
    println!("the number of non-free products: {products_nonfree}");

    if products_nonfree > 0 {
        println!(
            "the average price of non-free products: {}",
            f64::from(price) / f64::from(products_nonfree)
        );
    }

    let works_all = count_works(&conn, range, false).context("failed to count works")?;
    let works_nonfree =
        count_works(&conn, range, true).context("failed to count non-free works")?;
    println!("the number of works: {works_all}");
    println!("the number of non-free works: {works_nonfree}");

    if works_nonfree > 0 {
        println!(
            "the average price of non-free works: {}",
            f64::from(price) / f64::from(works_nonfree)
        );
    }

    Ok(())
}

fn consumed_price<T>(conn: &rusqlite::Connection, range: T) -> Result<u32>
where
    T: RangeBounds<OffsetDateTime>,
{
    consumed_price_impl(
        conn,
        range.start_bound().cloned(),
        range.end_bound().cloned(),
    )
}

fn consumed_price_impl(
    conn: &rusqlite::Connection,
    start: Bound<OffsetDateTime>,
    end: Bound<OffsetDateTime>,
) -> Result<u32> {
    let (start_bound, start) = match start {
        Bound::Unbounded => (0, None),
        Bound::Included(start) => (1, Some(start)),
        Bound::Excluded(start) => (2, Some(start)),
    };
    let (end_bound, end) = match end {
        Bound::Unbounded => (0, None),
        Bound::Included(end) => (1, Some(end)),
        Bound::Excluded(end) => (2, Some(end)),
    };
    let price = conn
        .query_row(
            "SELECT IFNULL(SUM(purchase_price), 0) FROM products WHERE \
             CASE WHEN :start_bound = 0 THEN 1 \
             WHEN :start_bound = 1 THEN IFNULL(purchased_at >= :start, 1) \
             ELSE IFNULL(purchased_at > :start, 1) END AND \
             CASE WHEN :end_bound = 0 THEN 1 \
             WHEN :end_bound = 1 THEN IFNULL(purchased_at <= :end, 1) \
             ELSE IFNULL(purchased_at < :end, 1) END",
            named_params! {
                ":start": start,
                ":start_bound": start_bound,
                ":end": end,
                ":end_bound": end_bound,
            },
            |row| row.get(0),
        )
        .context("failed to get consumed price")?;

    Ok(price)
}

fn count_products<T>(conn: &rusqlite::Connection, range: T, only_nonfree: bool) -> Result<u32>
where
    T: RangeBounds<OffsetDateTime>,
{
    count_products_impl(
        conn,
        range.start_bound().cloned(),
        range.end_bound().cloned(),
        only_nonfree,
    )
}

fn count_products_impl(
    conn: &rusqlite::Connection,
    start: Bound<OffsetDateTime>,
    end: Bound<OffsetDateTime>,
    only_nonfree: bool,
) -> Result<u32> {
    let (start_bound, start) = match start {
        Bound::Unbounded => (0, None),
        Bound::Included(start) => (1, Some(start)),
        Bound::Excluded(start) => (2, Some(start)),
    };
    let (end_bound, end) = match end {
        Bound::Unbounded => (0, None),
        Bound::Included(end) => (1, Some(end)),
        Bound::Excluded(end) => (2, Some(end)),
    };
    let count = conn
        .query_row(
            "SELECT COUNT(*) FROM products WHERE \
             CASE WHEN :start_bound = 0 THEN 1 \
             WHEN :start_bound = 1 THEN IFNULL(purchased_at >= :start, 1) \
             ELSE IFNULL(purchased_at > :start, 1) END AND \
             CASE WHEN :end_bound = 0 THEN 1 \
             WHEN :end_bound = 1 THEN IFNULL(purchased_at <= :end, 1) \
             ELSE IFNULL(purchased_at < :end, 1) END AND \
             IIF(:only_nonfree <> 0, purchase_price <> 0, 1)",
            named_params! {
                ":start": start,
                ":start_bound": start_bound,
                ":end": end,
                ":end_bound": end_bound,
                ":only_nonfree": only_nonfree,
            },
            |row| row.get(0),
        )
        .context("failed to get consumed price")?;

    Ok(count)
}

fn count_works<T>(conn: &rusqlite::Connection, range: T, only_nonfree: bool) -> Result<u32>
where
    T: RangeBounds<OffsetDateTime>,
{
    count_works_impl(
        conn,
        range.start_bound().cloned(),
        range.end_bound().cloned(),
        only_nonfree,
    )
}

fn count_works_impl(
    conn: &rusqlite::Connection,
    start: Bound<OffsetDateTime>,
    end: Bound<OffsetDateTime>,
    only_nonfree: bool,
) -> Result<u32> {
    let (start_bound, start) = match start {
        Bound::Unbounded => (0, None),
        Bound::Included(start) => (1, Some(start)),
        Bound::Excluded(start) => (2, Some(start)),
    };
    let (end_bound, end) = match end {
        Bound::Unbounded => (0, None),
        Bound::Included(end) => (1, Some(end)),
        Bound::Excluded(end) => (2, Some(end)),
    };
    let count = conn
        .query_row(
            "SELECT COUNT(*) FROM works \
             INNER JOIN works_in_product USING(work_internal_id) \
             INNER JOIN products USING(product_userbuy_index) \
             WHERE \
             CASE WHEN :start_bound = 0 THEN 1 \
             WHEN :start_bound = 1 THEN IFNULL(purchased_at >= :start, 1) \
             ELSE IFNULL(purchased_at > :start, 1) END AND \
             CASE WHEN :end_bound = 0 THEN 1 \
             WHEN :end_bound = 1 THEN IFNULL(purchased_at <= :end, 1) \
             ELSE IFNULL(purchased_at < :end, 1) END AND \
             IIF(:only_nonfree <> 0, purchase_price <> 0, 1)",
            named_params! {
                ":start": start,
                ":start_bound": start_bound,
                ":end": end,
                ":end_bound": end_bound,
                ":only_nonfree": only_nonfree,
            },
            |row| row.get(0),
        )
        .context("failed to get consumed price")?;

    Ok(count)
}
