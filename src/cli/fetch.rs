//! `fetch` subcommand.

use anyhow::{Context as _, Result};
use futures_util::stream::{self, StreamExt};
use time::OffsetDateTime;

use crate::client::{Client, PurchasedProductsPage, PurchasedWorksPage};
use crate::db::DbPool;

pub async fn run(client: Client, db: DbPool, refetch_all: bool) -> Result<()> {
    const FETCH_CONCURRENCY: usize = 5;

    let (skip_products, after) = if refetch_all {
        (0, None)
    } else {
        tokio::task::block_in_place({
            let conn = db.connect()?;
            move || {
                let state = conn
                    .query_row(
                        "SELECT next_userbuy_index, latest_fetched_works_datetime \
                         FROM dlsiter_state LIMIT 1",
                        [],
                        |row| {
                            let skip_products: u32 = row
                                .get("next_userbuy_index")
                                .expect("the field must be available");
                            let after: Option<OffsetDateTime> = row
                                .get("latest_fetched_works_datetime")
                                .expect("the field must be available");
                            Ok((skip_products, after))
                        },
                    )
                    .context("failed to load dlsiter state from the database")?;

                anyhow::Ok(state)
            }
        })?
    };
    log::debug!("the number of the skipped products is {skip_products}");
    let mut next_userbuy_index = skip_products;
    let mut latest_fetched_works_datetime = after.unwrap_or(OffsetDateTime::UNIX_EPOCH);

    // Fetch the purchased products.
    {
        let stream_products =
            PurchasedProductsPage::fetch_pages_stream(client.clone(), skip_products).await?;
        let tasks_products = stream::iter(stream_products).buffer_unordered(FETCH_CONCURRENCY);
        tokio::pin!(tasks_products);
        while let Some(task) = tasks_products.next().await {
            let page = task.context("failed to fetch a purchased products page")?;
            let page0 = page.page0;
            let products = page.products;

            if let Some(last_product) = products.last() {
                let last_userbuy_index = last_product
                    .index0
                    .expect("`fetch_pages_stream` must set userbuy indices");
                next_userbuy_index = next_userbuy_index.max(last_userbuy_index);
            }

            tokio::task::spawn_blocking({
                let db = db.clone();
                move || {
                    db.register_purchased_products(&products)?;

                    anyhow::Ok(())
                }
            })
            .await
            .with_context(|| {
                format!(
                    "failed to spawn a task for fetching purchased products page (page0={page0})"
                )
            })?
            .with_context(|| format!("failed to fetch purchased products page (page0={page0})"))?;
        }
    }

    // Fetch purchases data.
    {
        let stream_works = PurchasedWorksPage::fetch_pages_stream(client.clone(), after).await?;
        let tasks_works = stream::iter(stream_works).buffer_unordered(FETCH_CONCURRENCY);
        tokio::pin!(tasks_works);
        while let Some(task) = tasks_works.next().await {
            let page = task.context("failed to fetch a purchased works page")?;
            let page0 = page.page0;
            let works = page.works;

            let latest_purchase_datetime = works.iter().map(|work| work.purchased_at).max();
            if let Some(latest_purchase_datetime) = latest_purchase_datetime {
                latest_fetched_works_datetime =
                    latest_fetched_works_datetime.max(latest_purchase_datetime);
            }

            tokio::task::spawn_blocking({
                let db = db.clone();
                move || {
                    db.register_purchased_works(&works)?;

                    anyhow::Ok(())
                }
            })
            .await
            .with_context(|| {
                format!("failed to spawn a task for fetching purchased works page (page0={page0})")
            })?
            .with_context(|| format!("failed to fetch purchased works page (page0={page0})"))?;
        }
    }

    // Use precise datetimes when possible, even if the product is discontinued.
    {
        tokio::task::spawn_blocking({
            let db = db.clone();
            move || db.infer_precise_purchase_datetime()
        })
        .await
        .context("failed to spawn a task for inferring precise purchase datetimes")?
        .context("error happened during inferring precise purchase datetimes")?;
    }

    log::debug!("next `userbuy_index` is {next_userbuy_index}");
    tokio::task::block_in_place(move || {
        let conn = db.connect()?;
        conn.execute(
            "UPDATE dlsiter_state SET \
             next_userbuy_index = :next_userbuy_index, \
             latest_fetched_works_datetime = :latest_fetched_works_datetime",
            rusqlite::named_params! {
                ":next_userbuy_index": next_userbuy_index,
                ":latest_fetched_works_datetime": Some(latest_fetched_works_datetime),
            },
        )
        .context("failed to save dlsiter state to the database")?;

        anyhow::Ok(())
    })?;

    Ok(())
}
