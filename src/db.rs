//! Data store.

use std::collections::HashSet;
use std::fmt;
use std::path::Path;
use std::sync::Arc;

use anyhow::{anyhow, bail, Context as _, Result};
use r2d2::ManageConnection as _;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::{named_params, OptionalExtension as _};

use crate::client::{PurchasedProduct, PurchasedWork, WorkInProduct};

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!("./sqlite3_migrations");
}

/// Database connection.
#[derive(Debug, Clone)]
pub struct DbPool {
    /// Database connection pool.
    pool: Arc<SqliteConnectionManager>,
}

impl DbPool {
    /// Connects to the DB if available, or initialize DB if not exist.
    pub fn connect_or_init_database(path: &Path) -> Result<DbPool> {
        let pool = SqliteConnectionManager::file(path).with_init(|conn| {
            // 33554432: 32 * 2^20.
            conn.execute_batch(concat!(
                "PRAGMA mmap_size = 33554432;",
                "PRAGMA foreign_keys = true;"
            ))
        });

        // Initialize the schema.
        let mut conn = pool
            .connect()
            .with_context(|| format!("failed to connect to the DB {}", path.display()))?;
        log::debug!("Running migration for {}", path.display());
        embedded::migrations::runner()
            .run(&mut conn)
            .with_context(|| format!("failed to migrate {}", path.display()))?;
        log::debug!("Successfully migrated {}", path.display());

        Ok(Self {
            pool: Arc::new(pool),
        })
    }

    /// Obtains the connection exclusively.
    pub fn connect(&self) -> Result<rusqlite::Connection> {
        self.pool.connect().map_err(Into::into)
    }
}

impl DbPool {
    pub fn register_purchased_works(&self, works: &[PurchasedWork]) -> Result<()> {
        let mut conn = self.connect()?;
        let conn = conn.transaction().context("failed to start transaction")?;

        // Register makers.
        {
            log::trace!("start registering makers for purchased works");
            let mut stmt = conn
                .prepare_cached(
                    "INSERT INTO makers(maker_id, name) VALUES (:id, :name) \
                     ON CONFLICT DO UPDATE SET name = excluded.name",
                )
                .context("failed to prepare cached INSERT statement for makers")?;
            for maker in works.iter().map(|work| &work.maker) {
                log::trace!("registering the maker {:?}", maker.id);
                stmt.execute(named_params! {
                    ":id": maker.id,
                    ":name": maker.name.any_value(),
                })
                .with_context(|| format!("failed to register maker {:?}", maker.id))?;
            }
        }

        // Register work types.
        {
            log::trace!("start registering work types for purchased works");
            let mut stmt = conn
                .prepare_cached(
                    "INSERT INTO work_types(code) VALUES (:code) \
                     ON CONFLICT DO NOTHING",
                )
                .context("failed to prepare cached INSERT statement for work types")?;
            let work_types = works
                .iter()
                .map(|work| &work.work_type)
                .collect::<HashSet<_>>();
            for code in work_types {
                log::trace!("registering the work type {:?}", code);
                stmt.execute([code])
                    .with_context(|| format!("failed to register work type {code:?}"))?;
            }
        }

        // Register works.
        {
            log::trace!("start registering works for purchased works");
            let mut stmt = conn
                .prepare_cached(
                    "INSERT INTO works( \
                     work_id, name, registered_at, thumbnail_uri_large, thumbnail_uri_small,
                     work_type_internal_id, maker_internal_id \
                     ) SELECT
                     :work_id, :name, :registered_at, :thumbnail_uri_large, :thumbnail_uri_small, \
                     work_types.work_type_internal_id, makers.maker_internal_id \
                     FROM work_types, makers \
                     WHERE work_types.code = :work_type AND \
                     makers.maker_id = :maker_id \
                     ON CONFLICT DO UPDATE SET \
                     name = excluded.name, registered_at = excluded.registered_at, \
                     thumbnail_uri_large = excluded.thumbnail_uri_large, \
                     thumbnail_uri_small = excluded.thumbnail_uri_small, \
                     work_type_internal_id = excluded.work_type_internal_id, \
                     maker_internal_id = excluded.maker_internal_id",
                )
                .context("failed to prepare cached INSERT statement for works")?;
            for work in works {
                log::trace!("registering the work {:?}", work.work_id);
                stmt.execute(named_params! {
                    ":work_id": work.work_id,
                    ":name": work.name.any_value(),
                    ":registered_at": work.registered_at,
                    ":thumbnail_uri_large": work.thumbnails.large,
                    ":thumbnail_uri_small": work.thumbnails.small,
                    ":work_type": work.work_type,
                    ":maker_id": work.maker.id,
                })
                .with_context(|| format!("failed to register work {:?}", work.work_id))?;
            }
        }

        // Update purchase datetime if the corresponding product is alreday regisetred.
        //
        // Note that we do not register relations between works and products here.
        // Products may have associated works without known IDs, so they registers
        // the works at the same time, but **only when any relations are not yet
        // registered** to avoid such "anonymous" works are registered multiple
        // times.
        // Registering relations will prevent this strategy from working.
        {
            log::trace!("start updating purchase datetime to more precise values");
            let mut stmt = conn
                .prepare_cached(
                    "UPDATE products SET purchased_at = :purchased_at, \
                     purchase_datetime_is_precise = 1 \
                     FROM works INNER JOIN works_in_product \
                     USING (work_internal_id) \
                     WHERE (products.product_userbuy_index = \
                     works_in_product.product_userbuy_index) AND \
                     (works.work_id = :work_id) AND \
                     (products.purchase_datetime_is_precise = 0)",
                )
                .context("failed to prepare cached UPDATE statement for purchase datetimes")?;
            for work in works {
                log::trace!(
                    "updating the purchase datetime for the work {:?} to {}",
                    work.work_id,
                    work.purchased_at
                );
                stmt.execute(named_params! {
                    ":purchased_at": work.purchased_at,
                    ":work_id": work.work_id,
                })
                .with_context(|| format!("failed to register work {:?}", work.work_id))?;
            }
        }

        conn.commit().context("failed to commit the transaction")?;

        anyhow::Ok(())
    }

    pub fn register_purchased_products(&self, products: &[PurchasedProduct]) -> Result<()> {
        let mut conn = self.connect()?;
        let conn = conn.transaction().context("failed to start transaction")?;

        // Register linked makers.
        {
            log::trace!("start registering makers for purchased products");
            let mut stmt = conn
                .prepare_cached(
                    "INSERT INTO makers(maker_id, name) VALUES (:id, :name) \
                     ON CONFLICT DO UPDATE SET name = excluded.name",
                )
                .context("failed to prepare cached INSERT statement for makers")?;
            for product in products {
                log::trace!("registering the maker {:?}", product.maker_id);
                stmt.execute(named_params! {
                    ":id": product.maker_id,
                    ":name": product.maker_name,
                })
                .with_context(|| format!("failed to register maker {:?}", product.maker_id))?;
            }
        }

        // Register products.
        {
            log::trace!("start registering products for purchased products");
            // TODO: Register precise purchase datetime if available.
            let mut stmt = conn
                .prepare_cached(
                    "INSERT INTO products( \
                     product_userbuy_index, product_id, name, purchased_at, \
                     purchase_datetime_is_precise, purchase_price \
                     ) VALUES ( \
                     :product_userbuy_index, :product_id, :name, :purchased_at, \
                     :purchase_datetime_is_precise, :purchase_price \
                     ) ON CONFLICT DO UPDATE SET \
                     product_id = COALESCE(product_id, excluded.product_id), \
                     name = excluded.name, \
                     purchased_at = MAX(purchased_at, excluded.purchased_at), \
                     purchase_datetime_is_precise = MAX(purchase_datetime_is_precise, \
                     excluded.purchase_datetime_is_precise), \
                     purchase_price = excluded.purchase_price \
                     WHERE (product_id IS NULL) OR (product_id = excluded.product_id)",
                )
                .context("failed to prepare cached INSERT statement for makers")?;
            for product in products {
                let index0 = match product.index0 {
                    Some(v) => v,
                    None => {
                        bail!("products to be registerd must contain index in the purchase list")
                    }
                };
                log::trace!("registering the product {:?}", product.product_id);
                stmt.execute(named_params! {
                    ":product_userbuy_index": index0,
                    ":product_id": product.product_id,
                    ":name": product.title,
                    ":purchased_at": product.purchased_at,
                    ":purchase_datetime_is_precise": false,
                    ":purchase_price": product.purchase_price,
                })
                .with_context(|| format!("failed to register product {:?}", product.product_id))?;
            }
        }

        // Register linked works and their relations to products.
        {
            log::trace!("start registering works for purchased products");
            let mut stmt_count_rels = conn.prepare_cached(
                "SELECT COUNT(*) FROM products \
                 INNER JOIN works_in_product USING(product_userbuy_index) \
                 WHERE products.product_userbuy_index = :userbuy_index",
            )?;
            let mut stmt_work = conn
                .prepare_cached(
                    "INSERT INTO works(work_id, name, maker_internal_id) \
                     SELECT :work_id, :name, makers.maker_internal_id \
                     FROM makers WHERE makers.maker_id = :maker_id \
                     ON CONFLICT DO UPDATE SET name = excluded.name, \
                     maker_internal_id = COALESCE(maker_internal_id, \
                     excluded.maker_internal_id)",
                )
                .context("failed to prepare cached INSERT statement for works")?;
            let mut stmt_rel_work_id = conn
                .prepare_cached(
                    "INSERT INTO works_in_product(product_userbuy_index, \
                     work_internal_id, work_index_in_product) \
                     SELECT products.product_userbuy_index, works.work_internal_id, \
                     :work_index_in_product \
                     FROM products, works WHERE products.product_id = :product_id \
                     AND works.work_id = :work_id \
                     ON CONFLICT DO NOTHING",
                )
                .context(
                    "failed to prepare cached INSERT statement for work and product relations",
                )?;
            let mut stmt_rel_work_internal_id = conn
                .prepare_cached(
                    "INSERT INTO works_in_product(product_userbuy_index, \
                     work_internal_id, work_index_in_product) \
                     SELECT products.product_userbuy_index, \
                     :work_internal_id, :work_index_in_product \
                     FROM products WHERE products.product_id = :product_id \
                     ON CONFLICT DO NOTHING",
                )
                .context(
                    "failed to prepare cached INSERT statement for work and product relations",
                )?;

            for product in products {
                let userbuy_index = product.index0.expect(
                    "[consistency] product must have been confirmed to \
                     contain the index in the purchase list",
                );

                // First, check if the relations already exists.
                let num_rels: u32 = stmt_count_rels
                    .query_row(
                        named_params! {
                            ":userbuy_index": userbuy_index,
                        },
                        |row| row.get(0),
                    )
                    .with_context(|| {
                        format!(
                            "failed to get the number of relations for the product \
                             (id={:?}, userbuy_index={userbuy_index})",
                            product.product_id
                        )
                    })?;
                if num_rels != 0 {
                    // Relations seems to have already been registered.
                    // To prevent multiple identical relations for works with
                    // unknown work ID (i.e. NULL), avoid registering or
                    // updating relations multiple times for the same product.
                    log::trace!(
                        "relations are already registered for the product \
                         (num_rels={num_rels}, id={:?}, userbuy_index={userbuy_index})",
                        product.product_id
                    );
                    continue;
                }

                for (index_in_product, work) in product.works.iter().enumerate() {
                    #[derive(Clone, Copy)]
                    struct EntryDisplay<'a> {
                        index_in_product: usize,
                        work: &'a WorkInProduct,
                        userbuy_index: u32,
                        product: &'a PurchasedProduct,
                    }
                    impl fmt::Display for EntryDisplay<'_> {
                        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                            write!(f, "the {}-th work (", self.index_in_product + 1)?;
                            match &self.work.id {
                                Some(work_id) => write!(f, "id={work_id:?}")?,
                                None => write!(f, "name={:?}", self.work.name)?,
                            }
                            f.write_str(") of the product (")?;
                            match &self.product.product_id {
                                Some(product_id) => write!(f, "id={product_id:?})")?,
                                None => write!(
                                    f,
                                    "userbuy_index={}, name={:?})",
                                    self.userbuy_index, self.product.title
                                )?,
                            }
                            Ok(())
                        }
                    }
                    let entry_display = EntryDisplay {
                        index_in_product,
                        work,
                        userbuy_index,
                        product,
                    };

                    // Repurpose the product ID as a work ID, if necessary.
                    let work_id: Option<&str> = if let Some(work_id) = &work.id {
                        // Work ID is available. Use it.
                        Some(work_id)
                    } else if product.works.len() > 1 {
                        // The product is a bundle of multiple works.
                        // Do not repurpose the product ID.
                        work.id.as_deref()
                    } else {
                        // Work ID is not available, and the product only
                        // contains the work. Repurpose the product ID as a
                        // work ID.
                        product.product_id.as_deref()
                    };

                    log::trace!("registering {entry_display}");
                    let upserted_work_internal_id: Option<u32> = stmt_work
                        .query_row(
                            named_params! {
                                ":work_id": work_id,
                                ":name": work.name,
                                ":maker_id": product.maker_id,
                            },
                            |row| row.get::<_, u32>("work_internal_id"),
                        )
                        .optional()
                        .with_context(|| format!("failed to register {entry_display}"))?;
                    if let Some(work_id) = work_id {
                        log::trace!("registering the relation for {entry_display} using work ID");
                        stmt_rel_work_id
                            .execute(named_params! {
                                ":product_id": product.product_id,
                                ":work_id": work_id,
                                ":work_index_in_product": index_in_product,
                            })
                            .with_context(|| {
                                format!("failed to register the relation for {entry_display}")
                            })?;
                    } else {
                        // Work ID is unknown, but a new row for the work (with
                        // work ID `NULL`) is inserted.
                        assert_eq!(
                            num_rels, 0,
                            "the DB had not had associated works for the product"
                        );
                        // A new row must have been inserted, so this won't be `None`.
                        let work_internal_id = upserted_work_internal_id.ok_or_else(|| {
                            anyhow!("a row for the work without the ID must have been inserted")
                        })?;
                        log::trace!(
                            "registering the relation for {entry_display} using \
                             internal work ID {work_internal_id:?}"
                        );
                        stmt_rel_work_internal_id
                            .execute(named_params! {
                                ":userbuy_index": userbuy_index,
                                ":work_internal_id": work_internal_id,
                                ":work_index_in_product": index_in_product,
                            })
                            .with_context(|| {
                                format!("failed to register relations for {entry_display}")
                            })?;
                    }
                }
            }
        }

        conn.commit().context("failed to commit the transaction")?;

        Ok(())
    }

    pub fn infer_precise_purchase_datetime(&self) -> Result<()> {
        let mut conn = self.connect()?;
        let conn = conn.transaction().context("failed to start transaction")?;

        // Find precise purchase datetimes before and after the less precise purchase.
        {
            log::trace!("start registering makers for purchased products");
            let num_updated = conn
                .execute(
                    "UPDATE products \
                     SET purchased_at = prev_precise.purchased_at, \
                     purchase_datetime_is_precise = 1 \
                     FROM products AS prev_precise, products AS next_precise \
                     WHERE (products.purchase_datetime_is_precise = 0) \
                     AND (prev_precise.product_userbuy_index < products.product_userbuy_index) \
                     AND (products.product_userbuy_index < next_precise.product_userbuy_index) \
                     AND (prev_precise.purchase_datetime_is_precise <> 0) \
                     AND (next_precise.purchase_datetime_is_precise <> 0) \
                     AND (prev_precise.purchased_at = next_precise.purchased_at)",
                    [],
                )
                .context("failed to infer precise purchase datetimes")?;
            log::debug!("Inferred precise purchase datetimes of {num_updated} products");
        }

        conn.commit().context("failed to commit the transaction")?;

        Ok(())
    }
}
