//! DLsite client.

mod bought_product;
mod localized_string;
mod purchases;
mod session;
mod userbuy;
mod work_count;

use std::sync::Arc;

use anyhow::{anyhow, Context as _, Result};
use reqwest::cookie::Jar;
use reqwest::Client as HttpClient;

pub use self::bought_product::PurchasedProductList;
pub use self::localized_string::LocalizedString;
pub use self::purchases::{
    PurchasedWork, PurchasedWorkMaker, PurchasedWorkThumbnails, PurchasedWorksPage,
};
pub use self::session::SessionToken;
pub use self::userbuy::{
    PurchasedProduct, PurchasedProductsPage, PurchasedProductsPageQuery, WorkInProduct,
};
pub use self::work_count::WorkCount;

/// DLsite client.
///
/// Note that this client type does not guarantee that the session is valid.
#[derive(Debug, Clone)]
pub struct Client {
    /// HTTP client.
    http_client: HttpClient,
}

impl Client {
    /// Creates a client from the session token.
    pub fn new(token: &SessionToken) -> Result<Self> {
        let jar = Jar::default();
        token.add_to_jar(&jar);
        let http_client = HttpClient::builder()
            .cookie_provider(Arc::new(jar))
            .build()?;

        Ok(Self { http_client })
    }

    /// Checks whether the client successfully logged in.
    ///
    /// Returns `Ok(true)` when logged in, `Ok(false)` if not logged in,
    /// `Err(_)` if network error happened.
    pub async fn check_session(&self) -> Result<bool> {
        /// URI to check the session validity.
        // This API must return JSON data if the session is valid.
        const URI: &str = "https://play.dlsite.com/api/product_count";

        let resp = self.http_client.get(URI).send().await?.error_for_status()?;
        let content_type = resp
            .headers()
            .get(reqwest::header::CONTENT_TYPE)
            .ok_or_else(|| {
                anyhow!("unexpected response: no content-type header in the response for <{URI}>")
            })?
            .to_str()
            .context("invalid content-type header value")?;
        let is_json = content_type.starts_with("application/json");
        Ok(is_json)
    }

    /// Logs out from DLsite.
    ///
    /// Returns `Ok(())` if successfully logged out or already logged out (i.e.
    /// not logged in).
    pub async fn logout(&self) -> Result<()> {
        /// URI of the logout page.
        const LOGOUT_PAGE: &str = "https://www.dlsite.com/home/logout";

        let _ = self
            .http_client
            .get(LOGOUT_PAGE)
            .send()
            .await?
            .error_for_status()
            .context("failed to GET logout page")?;
        log::debug!("Logged out");

        Ok(())
    }

    /// Returns a reference to the internal HTTP client.
    #[inline]
    #[must_use]
    fn http_client(&self) -> &HttpClient {
        &self.http_client
    }
}

mod rfc3339_opt {
    use std::fmt;

    use time::OffsetDateTime;

    #[derive(serde::Serialize)]
    #[serde(transparent)]
    struct Rfc3339DateTime(#[serde(with = "time::serde::rfc3339")] OffsetDateTime);

    /// Serializes optional RFC 3339 string.
    pub fn serialize<S>(datetime: &Option<OffsetDateTime>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match datetime {
            Some(datetime) => serializer.serialize_some(&Rfc3339DateTime(*datetime)),
            None => serializer.serialize_none(),
        }
    }

    #[derive(Clone, Copy)]
    struct Visitor;

    impl<'de> serde::de::Visitor<'de> for Visitor {
        type Value = Option<OffsetDateTime>;

        fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            f.write_str("optional RFC 3339 datetime")
        }

        fn visit_none<E>(self) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            Ok(None)
        }

        fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            use serde::de::Error;

            let s = <Option<&str> as serde::Deserialize>::deserialize(deserializer)?;
            let s = match s {
                Some(s) => s,
                None => return Ok(None),
            };
            match OffsetDateTime::parse(s, &time::format_description::well_known::Rfc3339) {
                Ok(v) => Ok(Some(v)),
                Err(_) => Err(D::Error::invalid_value(
                    serde::de::Unexpected::Str(s),
                    &self,
                )),
            }
        }
    }

    /// Deserializes optional RFC 3339 string.
    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<OffsetDateTime>, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_option(Visitor)
    }
}
