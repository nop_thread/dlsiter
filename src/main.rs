#![forbid(unsafe_code)]
#![warn(rust_2018_idioms)]
#![warn(clippy::must_use_candidate)]
#![warn(clippy::unwrap_used)]

mod cli;
pub mod client;
mod config;
mod db;

use anyhow::{bail, Context as _, Result};

use crate::cli::{Cli, Command};
use crate::client::{Client, SessionToken};

/// Entrypoint.
#[tokio::main]
async fn main() -> Result<()> {
    init_logger();

    let cli = Cli::parse()?;

    match cli.command() {
        Command::Login {
            check: true,
            forget_id_password,
            ..
        } => {
            let client = create_client(&cli)
                .await
                .context("failed to create DLsite client")?;
            if client.is_some() {
                log::info!("Valid session available");
            } else {
                log::info!("No valid session available");
            }

            if *forget_id_password {
                // Modify secrets.
                if let Some(mut secrets) = cli.load_secrets().context("failed to load secrets")? {
                    secrets.login_credentials = None;
                    log::info!("Removing credentials from secrets file");

                    // Save the secrets.
                    let secrets_path = cli.secrets_path();
                    secrets.save_to_path(&secrets_path).with_context(|| {
                        format!("failed to save secrets to the file {secrets_path:?}")
                    })?;
                    log::debug!("Saved secrets to {secrets_path:?}")
                }
            }
        }
        Command::Login {
            check: false,
            save_id_password,
            forget_id_password,
        } => {
            let (retrieved_from_secrets_file, mut secrets) = cli
                .load_or_init_secrets()
                .context("failed to load or init secrets")?;
            // Log in if not yet logged in.
            if secrets.session_token().is_none() {
                log::debug!("Session token is not available");

                let creds = secrets
                    .login_credentials()
                    .expect("must have been loaded credentials successfully");
                let token = SessionToken::login(creds)
                    .await
                    .context("failed to log in to DLsite")?;
                log::debug!("Got a session token");
                secrets.session_token = Some(token);
            }
            let session_token = secrets
                .session_token()
                .expect("session token must have been set");

            // Test if the session token is valid.
            let client = Client::new(session_token)?;
            if !client.check_session().await? {
                bail!("failed to log in");
            }

            // Modify secrets.
            debug_assert!(
                !(*save_id_password && *forget_id_password),
                "CLI flags to save and to forget are exclusive"
            );
            let loaded_from_terminal_and_not_told_to_save =
                !retrieved_from_secrets_file && !*save_id_password;
            if *forget_id_password || loaded_from_terminal_and_not_told_to_save {
                secrets.login_credentials = None;
                log::info!("Removing credentials from secrets file");
            }
            // Save the secrets.
            let secrets_path = cli.secrets_path();
            secrets
                .save_to_path(&secrets_path)
                .with_context(|| format!("failed to save secrets to the file {secrets_path:?}"))?;
            log::debug!("Saved secrets to {secrets_path:?}")
        }
        Command::Logout => {
            match create_client(&cli).await {
                Ok(Some(client)) => {
                    if let Err(e) = client.logout().await {
                        log::info!("Session has already been expired: {e}");
                    } else {
                        log::info!("Successfully logged out");
                    }
                }
                Ok(None) => {
                    log::info!("Not logged in");
                }
                Err(e) => {
                    log::info!(
                        "failed to create client (maybe the session \
                         has already been expired): {e}"
                    );
                }
            }

            // Update the secrets.
            let mut secrets = cli
                .load_secrets()?
                .expect("should success since the login session token must be available");
            secrets.session_token = None;

            let secrets_path = cli.secrets_path();
            secrets
                .save_to_path(&secrets_path)
                .with_context(|| format!("failed to update secrets at {secrets_path:?}"))?;
            log::debug!("Updated secrets at {secrets_path:?}")
        }
        Command::Fetch {
            database,
            refetch_all,
        } => {
            let client = create_client(&cli)
                .await
                .context("failed to create DLsite client")?;
            let client = match client {
                Some(v) => v,
                None => {
                    eprintln!("Not logged in. Log in by `login` subcommand.");
                    std::process::exit(1);
                }
            };

            let database = cli.connect_or_init_database(database.as_deref())?;

            cli::fetch::run(client, database, *refetch_all)
                .await
                .context("`fetch` subcommand failed")?;
        }
        Command::Stats { database, opts } => {
            let database = cli.connect_or_init_database(database.as_deref())?;

            cli::stats::run(&database, opts).context("`stats` subcommand failed")?;
        }
    }

    Ok(())
}

/// Initialize logger.
fn init_logger() {
    /// Default log filter for debug build.
    #[cfg(debug_assertions)]
    const DEFAULT_LOG_FILTER: &str = "dlsiter=debug";
    /// Default log filter for release build.
    #[cfg(not(debug_assertions))]
    const DEFAULT_LOG_FILTER: &str = "dlsiter=warn";

    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or(DEFAULT_LOG_FILTER))
        .filter_module("html5ever", log::LevelFilter::Off)
        .filter_module("selectors", log::LevelFilter::Off)
        .init();
}

/// Creates the DLsite client.
///
/// Returns `Ok(Some(client))` when successfully created, `Ok(None)` when there
/// are no available sessions, `Err(_)` when failed.
async fn create_client(cli: &Cli) -> Result<Option<Client>> {
    let secrets = match cli.load_secrets()? {
        Some(v) => v,
        None => {
            log::debug!("No secrets are available");
            return Ok(None);
        }
    };
    let session_token = match secrets.session_token() {
        Some(v) => v,
        None => {
            log::debug!("No session token is available");
            return Ok(None);
        }
    };

    // Test if the session token is valid.
    let client = Client::new(session_token)?;
    if !client.check_session().await? {
        eprintln!("Not logged in.");
        eprintln!("Run `login` subcommand to log in.");
        return Ok(None);
    }

    Ok(Some(client))
}
