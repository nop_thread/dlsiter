//! DLsite login session management.

use std::fmt;
use std::sync::Arc;

use anyhow::{anyhow, Context as _, Result};
use once_cell::sync::OnceCell;
use regex::Regex;
use reqwest::cookie::{CookieStore, Jar};
use reqwest::Url;
use serde::{Deserialize, Serialize};

use crate::config::LoginCredentials;

/// DLsite session token.
///
/// Note that this type does not guarantee that the session token is valid.
#[derive(Clone, Deserialize, Serialize)]
pub struct SessionToken {
    /// `DLsite_SID` cookie value.
    dlsite_sid: String,
}

impl SessionToken {
    /// Returns the URI for the session token cookie.
    #[must_use]
    fn cookie_url() -> &'static Url {
        /// URI for the session cookie.
        static DLSITE_COM: OnceCell<Url> = OnceCell::new();

        DLSITE_COM.get_or_init(|| {
            "https://dlsite.com/"
                .parse()
                .expect("should never fail since the URL is valid")
        })
    }

    /// Adds the session token cookie to the jar.
    pub fn add_to_jar(&self, jar: &Jar) {
        let cookie = format!(
            "__DLsite_SID={}; Domain=.dlsite.com; HttpOnly; Path=/",
            self.dlsite_sid
        );
        jar.add_cookie_str(&cookie, Self::cookie_url());
    }

    /// Extracts the session token from the cookie store.
    pub fn extract_from_cookie_store<C: CookieStore>(store: &C) -> Result<Self> {
        // Header for `dlsite.com`.
        let header = {
            store.cookies(Self::cookie_url()).ok_or_else(|| {
                anyhow!(
                    "expected a cookie for <{}>, but not found",
                    Self::cookie_url()
                )
            })?
        };
        let cookies_str = header
            .to_str()
            .with_context(|| format!("cookie value might be invalid (value={:?})", header))?;
        let dlsite_sid = cookies_str
            .split("; ")
            .find_map(|kv| kv.strip_prefix("__DLsite_SID="))
            .ok_or_else(|| {
                anyhow!("`__DLsite_SID` cookie not found, possibly due to login failure")
            })?
            .to_owned();

        Ok(Self { dlsite_sid })
    }

    /// Logs in to DLsite and creates a new session.
    pub async fn login(creds: &LoginCredentials) -> Result<Self> {
        /// URI of the login page.
        const LOGIN_PAGE: &str = "https://login.dlsite.com/login";
        /// URI of the login finish page.
        const LOGIN_FINISH_PAGE: &str = "https://www.dlsite.com/home/login/finish";

        let cookie_store = Arc::new(reqwest::cookie::Jar::default());
        let client = reqwest::Client::builder()
            .cookie_provider(cookie_store.clone())
            .build()?;

        // Get the login form, and POST the credentials.
        {
            // Get the login form.
            log::trace!("Getting login form");
            let login_page = client
                .get(LOGIN_PAGE)
                .send()
                .await?
                .error_for_status()
                .context("failed to GET login page")?;
            let html_text = login_page.text().await?;
            let token = extract_login_form_token(&html_text)?;
            log::trace!("Successfully retrieved login form");

            let login_form = LoginForm {
                token,
                login_id: creds.login_id(),
                password: creds.password(),
            };
            // Send the credentials to login.
            log::trace!("Submitting login form");
            client
                .post(LOGIN_PAGE)
                .form(&login_form)
                .send()
                .await?
                .error_for_status()
                .context("failed to POST login data")?;
            log::trace!("Successfully submitted login data");
        };

        // Finish the login.
        // `__DLsite_SID` cookie will be returned for this request.
        log::trace!("Finishing login");
        client
            .get(LOGIN_FINISH_PAGE)
            .send()
            .await?
            .error_for_status()
            .context("failed to finish login")?;
        log::info!("Successfully logged in");

        Self::extract_from_cookie_store(&*cookie_store)
    }
}

impl fmt::Debug for SessionToken {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SessionToken")
            .field("dlsite_sid", &"(secret)")
            .finish()
    }
}

/// Extracts the login form token from the HTML content.
fn extract_login_form_token(html: &str) -> Result<&str> {
    // Extract the entire `<input name="token" value="..." ...>` tag.
    let re_token_tag =
        Regex::new(r#"<input\s[^>]*name=["']_token["'][^>]*>"#).expect("must be valid regex");
    let token_tag = re_token_tag
        .find_iter(html)
        .next()
        .ok_or_else(|| anyhow!("failed to get login form token"))?
        .as_str();

    // Extract the value of the `value` attribute from the tag.
    let re_token_value = Regex::new(r#".*value=["']([^"']*)["'].*"#).expect("must be valid regex");
    let token = re_token_value
        .captures(token_tag)
        .ok_or_else(|| anyhow!("failed to get login form token"))?
        .get(1)
        .expect("the regex should have captured the sumbatch")
        .as_str();

    Ok(token)
}

/// Login form data.
#[derive(Serialize)]
struct LoginForm<'a> {
    /// Login form token retrieved from the login page.
    // `//form[action="https://login.dlsite.com/login"]/input[@name="_token"]/@value`
    #[serde(rename = "_token")]
    token: &'a str,
    /// Login ID.
    // `*[@id="form_id"]`
    login_id: &'a str,
    /// Password.
    // `*[@id="form_password"]`
    password: &'a str,
}
