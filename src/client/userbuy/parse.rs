//! Parsing of userbuy page.

use anyhow::{bail, Context as _, Result};
use once_cell::sync::OnceCell;
use scraper::{Html, Selector};
use time::{PrimitiveDateTime, UtcOffset};

use crate::client::{Client, PurchasedProduct, WorkInProduct};

/// HTML document of a userbuy page.
#[derive(Debug, Clone)]
pub(super) struct UserbuyDocument {
    html: Html,
}

impl UserbuyDocument {
    /// Fetches the userbuy page.
    pub(super) async fn fetch(client: &Client, uri: &str) -> Result<Self> {
        let resp = client
            .http_client()
            .get(uri)
            .send()
            .await
            .and_then(|v| v.error_for_status())
            .context("failed to retrieve userbuy data (uri=<{uri}>)")?;
        let html = resp
            .text()
            .await
            .context("failed to get text data for thew userbuy page (uri=<{uri}>)")?;
        let html = Html::parse_document(&html);

        Ok(Self { html })
    }

    /// Parses userbuy entries.
    pub(super) fn parse_items(&self, uri: &str) -> Result<Vec<PurchasedProduct>> {
        let selectors = Selectors::get();
        let items = self.html.select(&selectors.item);
        let items = items
            .enumerate()
            .filter_map(
                |(item_index, item)| match Self::parse_single_item(selectors, item) {
                    Ok(v) => Some(v),
                    Err(e) => {
                        let e = e.context(format!(
                            "failed to parse userbuy entry (uri=<{uri}>, index0={item_index})"
                        ));
                        log::error!("failed to parse userbuy item: {e:#}");
                        None
                    }
                },
            )
            .collect::<Vec<_>>();
        Ok(items)
    }

    /// Parses the number of userbuy pages.
    pub(super) fn parse_num_pages(&self, uri: &str) -> Result<u32> {
        let selectors = Selectors::get();
        let last_page_button_child = self.html.select(&selectors.last_page_button_child).next();
        let elem_ref = match last_page_button_child {
            Some(v) => v,
            None => {
                // No paginations. Number of pages is 0 or 1.
                // If the page has any items, there is only 1 page.
                // If not, no pages are available.
                if self.html.select(&selectors.item).next().is_some() {
                    return Ok(1);
                } else {
                    return Ok(0);
                }
            }
        };
        // If the current page is the last page, the element is the `<strong>`
        // tag and contains page number inside it.
        // If not the last page, the element is the `<a>` tag and contains the
        // page number by `data-value` attribute.
        let elem = elem_ref.value();
        match elem.attr("data-value").map(|s| s.trim()) {
            Some(num) => num.parse::<u32>().with_context(|| {
                format!("expected the page number for `data-value` attr but got {num:?} in <{uri}>")
            }),
            None => {
                let num = elem_ref.text().collect::<String>();
                let num = num.trim();
                num.parse::<u32>().with_context(|| {
                    format!(
                        "expected the page number in the {} element, but got {num:?} in <{uri}>",
                        elem.name()
                    )
                })
            }
        }
    }
}

impl UserbuyDocument {
    /// Parses a single item.
    fn parse_single_item(
        selectors: &Selectors,
        item: scraper::ElementRef<'_>,
    ) -> Result<PurchasedProduct> {
        let datetime = item
            .select(&selectors.purchase_datetime)
            .next()
            .context("unexpected userbuy data: purchase datetime was empty")?
            .inner_html();
        let datetime = datetime.trim();
        let datetime = PrimitiveDateTime::parse(datetime, &selectors.purchase_datetime_format)
            .with_context(|| format!("invalid purchase datetime {datetime:?}"))?
            .assume_offset(selectors.purchase_datetime_offset);

        let thumbnail_uri = item
            .select(&selectors.thumbnail_uri)
            .next()
            .and_then(|elem_ref| elem_ref.value().attr("src"))
            .map(|s| {
                let s = s.trim();
                if s.starts_with("//") {
                    // Transform protocol-relative URI to an absolute one.
                    format!("https:{}", s)
                } else {
                    s.to_owned()
                }
            });

        let purchase_price = item
            .select(&selectors.purchase_price)
            .next()
            .context("unexpected userbuy data: purchase price was empty")?
            .text()
            .flat_map(|frag| frag.chars())
            .filter(|c| c.is_ascii_digit())
            .collect::<String>();
        let purchase_price = purchase_price
            .parse::<u32>()
            .with_context(|| format!("failed to parse price {purchase_price:?}"))?;

        let maker_name = item
            .select(&selectors.maker_name)
            .next()
            .context("unexpected userbuy data: purchase price was empty")?
            .text()
            .collect::<String>();
        let maker_name = maker_name.trim().to_owned();

        let maker_id = item
            .select(&selectors.maker_id)
            .next()
            .and_then(|elem_ref| elem_ref.value().attr("href"))
            .and_then(extract_maker_id_from_page_uri)
            .map(ToOwned::to_owned);

        let title_elem = item
            .select(&selectors.product_name)
            .next()
            .context("item title not found")?;
        let title = title_elem.text().collect::<String>();
        let title = title.trim().to_owned();
        if title.is_empty() {
            bail!("title is unexpectedly empty");
        }

        let mut product_id = None;
        let mut works = item
            .select(&selectors.works_list)
            .map(|work_link_elem| {
                let id = work_link_elem
                    .value()
                    .attr("href")
                    .and_then(extract_work_id_from_page_uri)
                    .map(ToOwned::to_owned);
                let name = work_link_elem.text().collect::<String>().trim().to_owned();
                WorkInProduct { id, name }
            })
            .collect::<Vec<_>>();
        if works.is_empty() {
            // All of the bundled works are discontinued, or there is only singe work.
            let work_id = title_elem
                .select(&selectors.single_work_link)
                .next()
                .and_then(|work_link_elem| {
                    work_link_elem
                        .value()
                        .attr("href")
                        .and_then(extract_work_id_from_page_uri)
                        .map(ToOwned::to_owned)
                });
            let work = WorkInProduct {
                id: work_id.clone(),
                name: title.clone(),
            };
            works = vec![work];
            if work_id.is_some() {
                product_id = work_id;
            }
        }

        let download_page_uri = item
            .select(&selectors.download_page_uri)
            .next()
            .and_then(|a| a.value().attr("href"))
            .map(|s| s.trim())
            .map(ToOwned::to_owned);

        if product_id.is_none() || works.is_empty() {
            if let Some(id) = thumbnail_uri
                .as_deref()
                .and_then(extract_product_id_from_thumbnail_uri)
            {
                if product_id.is_none() {
                    product_id = Some(id.to_owned());
                }
                if works.is_empty() {
                    let work = WorkInProduct {
                        id: Some(id.to_owned()),
                        name: title.clone(),
                    };
                    works = vec![work];
                }
            }
        }

        Ok(PurchasedProduct {
            index0: None,
            purchased_at: datetime,
            thumbnail_uri,
            title,
            product_id,
            works,
            maker_id,
            maker_name,
            download_page_uri,
            purchase_price,
        })
    }
}

/// Selectors.
pub(super) struct Selectors {
    /// Selector for an item.
    pub(super) item: Selector,
    /// Purchase datetime (in minutes precision).
    pub(super) purchase_datetime: Selector,
    /// Purchase datetime format.
    pub(super) purchase_datetime_format: Vec<time::format_description::FormatItem<'static>>,
    /// Purchase datetime offset.
    pub(super) purchase_datetime_offset: UtcOffset,
    /// Thumbnail image URI.
    pub(super) thumbnail_uri: Selector,
    /// Product name.
    pub(super) product_name: Selector,
    /// Anchor that contains the item name.
    pub(super) single_work_link: Selector,
    /// Works list.
    pub(super) works_list: Selector,
    /// Maker ID.
    pub(super) maker_id: Selector,
    /// Maker name.
    pub(super) maker_name: Selector,
    /// Download page URI.
    pub(super) download_page_uri: Selector,
    /// Purchase price.
    pub(super) purchase_price: Selector,
    /// Child node of the button to the last page.
    pub(super) last_page_button_child: Selector,
}

impl Selectors {
    /// Returns the selector for an item.
    #[must_use]
    fn get() -> &'static Self {
        /// Selector for the item.
        static SELECTORS: OnceCell<Selectors> = OnceCell::new();

        SELECTORS.get_or_init(Selectors::new)
    }

    fn new() -> Self {
        let item = Selector::parse("#buy_history_this >.work_list_main >tbody >tr:not(.item_name)")
            .expect("selector must be valid");
        let purchase_datetime = Selector::parse(".buy_date").expect("selector must be valid");
        /// Purchase datetime format.
        const DATETIME_FORMAT: &str = "[year]/[month]/[day] [hour repr:24]:[minute]";
        let purchase_datetime_format = time::format_description::parse(DATETIME_FORMAT)
            .expect("datetime format description must be valid");
        // NOTE: `userbuy` page seems to always use `+09:00` offset even when
        // `?locale=en_US` is appended to URI. It is unknown which it is fixed
        // to, `+09:00` or the time offset at the time the user purchases.
        let purchase_datetime_offset = UtcOffset::from_hms(9, 0, 0).expect("valid datetime offset");
        let thumbnail_uri =
            Selector::parse(".work_1col_thumb .work_thumb img").expect("selector must be valid");
        let product_name = Selector::parse(".work_name").expect("selector must be valid");
        let single_work_link = Selector::parse("a").expect("selector must be valid");
        let works_list = Selector::parse(".work_content .work_notice ul.simple_list >li >a[href]")
            .expect("selector must be valid");
        let maker_id = Selector::parse(".maker_name >a").expect("selector must be valid");
        let maker_name = Selector::parse(".maker_name").expect("selector must be valid");
        let download_page_uri =
            Selector::parse(".re_dl a.btn_dl:not(.disabled)").expect("selector must be valid");
        let purchase_price = Selector::parse(".work_price").expect("selector must be valid");

        let last_page_button_child = Selector::parse("#unpaid_list .page_no ul >li:last-child >*")
            .expect("selector must be valid");

        Self {
            item,
            purchase_datetime,
            purchase_datetime_format,
            purchase_datetime_offset,
            thumbnail_uri,
            product_name,
            single_work_link,
            works_list,
            maker_id,
            maker_name,
            download_page_uri,
            purchase_price,
            last_page_button_child,
        }
    }
}

/// Extracts the last component from the DLsite page URI.
#[must_use]
fn extract_uri_last_component(uri: &str) -> Option<&str> {
    // URI will be `.../(DATA).html`, `.../(DATA)/`, or
    // `.../(DATA)` format.
    let uri = uri.trim().trim_end_matches('/');
    let last_slash = uri.rfind('/')?;
    let filename = &uri[(last_slash + 1)..];
    let id = filename
        .find('.')
        .map_or(filename, |first_period| &filename[..first_period]);
    Some(id)
}

/// Extracts work ID or product ID from the work page URI.
#[inline]
#[must_use]
fn extract_work_id_from_page_uri(uri: &str) -> Option<&str> {
    extract_uri_last_component(uri)
}

/// Extracts maker ID from the maker page URI.
#[inline]
#[must_use]
fn extract_maker_id_from_page_uri(uri: &str) -> Option<&str> {
    extract_uri_last_component(uri)
}

/// Extracts product ID from the thumbnail image URI.
fn extract_product_id_from_thumbnail_uri(uri: &str) -> Option<&str> {
    // Expected format: "https://img.dlsite.jp/.../{{PRODUCT_ID}}_img_main_{{SIZExSIZE}}.jpg",
    // if the thumbnail is available.
    let uri = uri.trim();
    let last_slash = uri.rfind('/')?;
    let filename = &uri[(last_slash + 1)..];
    let first_underscore = filename.find('_')?;
    Some(&filename[..first_underscore])
}
