//! `purchases` API.

use std::future::Future;
use std::num::NonZeroU32;

use anyhow::{Context as _, Result};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::client::{Client, LocalizedString, WorkCount};

/// A purchases page.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedWorksPage {
    /// The datetime the returned works are purchased after.
    ///
    /// Works in the page is bought after (and **excluding**) this datetime.
    #[serde(with = "time::serde::rfc3339")]
    pub last: OffsetDateTime,
    /// The maximum number of the returned works in a page.
    pub limit: NonZeroU32,
    /// Offset of the page, starting with zero.
    ///
    /// Note that `page` query in the URI is one-based, but this `offset` field
    /// is zero-based. `purchases?page=3` returns the page with `offset: 2`.
    #[serde(rename = "offset")]
    pub page0: u32,
    /// Works.
    pub works: Vec<PurchasedWork>,
}

impl PurchasedWorksPage {
    /// Fetches the purchases data.
    pub async fn fetch(client: &Client, page0: u32, after: Option<OffsetDateTime>) -> Result<Self> {
        /// Endpoint base for the purchases data.
        const URI_BASE: &str = "https://play.dlsite.com/api/purchases";

        let page1 = page0 + 1;
        let uri = match after {
            Some(after) => format!("{URI_BASE}?page={page1}&last={}", after.unix_timestamp()),
            None => format!("{URI_BASE}?page={page1}"),
        };
        let resp = client
            .http_client()
            .get(&uri)
            .send()
            .await
            .and_then(|v| v.error_for_status())
            .context("failed to retrieve purchases data")?;
        resp.json::<Self>()
            .await
            .with_context(|| format!("invalid purchases data (page0={page0}, after={after:?})"))
    }

    /// Returns an iterator of futures of `PurchasesPage`s.
    pub async fn fetch_pages_stream(
        client: Client,
        after: Option<OffsetDateTime>,
    ) -> Result<impl Iterator<Item = impl Future<Output = Result<PurchasedWorksPage>>>> {
        let product_count = WorkCount::fetch(&client, after).await.with_context(|| {
            format!(
                "failed to fetch product count after {:?}",
                after.map(|v| v.unix_timestamp())
            )
        })?;
        log::debug!(
            "The number of works after {:?}: {}",
            after.map(|time| time.to_string()),
            product_count.user
        );
        let num_purchases_pages: u32 = product_count.num_purchases_pages();
        log::debug!("The number of `purchases` pages to fetch: {num_purchases_pages}");

        Ok((0..num_purchases_pages).map(move |page0| {
            let after = after;
            let client = client.clone();
            let page1 = page0 + 1;
            async move {
                log::trace!("Fetching purchase page {page1} of {num_purchases_pages}");
                let page_data = PurchasedWorksPage::fetch(&client, page0, after)
                    .await
                    .with_context(|| {
                        format!("failed to fetch purchases page {page1} of {num_purchases_pages}")
                    })?;
                log::trace!("Successfully fetched purchase page {page1} of {num_purchases_pages}");

                Ok(page_data)
            }
        }))
    }
}

/// A purchased work.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedWork {
    /// Work ID.
    #[serde(rename = "workno")]
    pub work_id: String,
    /// The datetime the work is bought.
    #[serde(rename = "sales_date")]
    #[serde(with = "time::serde::rfc3339")]
    pub purchased_at: OffsetDateTime,
    /// (Unknown.)
    // TODO
    pub inservice: u8,
    /// Work name.
    pub name: LocalizedString,
    /// Phonetic of the name.
    ///
    /// All characters are in Katakana in Japanese.
    #[serde(deserialize_with = "LocalizedString::deserialize_opt")]
    pub name_phonetic: Option<LocalizedString>,
    /// Maker.
    pub maker: PurchasedWorkMaker,
    /// Work type.
    pub work_type: String,
    /// File type.
    pub file_type: String,
    /// Age category.
    pub age_category: String,
    /// DL format.
    pub dl_format: u32,
    /// Site ID.
    pub site_id: String,
    /// Content length.
    pub content_length: u64,
    /// Content count.
    pub content_count: u32,
    /// The datetime the work is registered to the site.
    ///
    /// This can be null for discontinued works.
    /// See `RJ345490` (which is not sold anymore) for example (if you have it).
    #[serde(rename = "regist_date")]
    #[serde(with = "crate::client::rfc3339_opt")]
    pub registered_at: Option<OffsetDateTime>,
    /// The datetime the work is last updated.
    #[serde(rename = "upgrade_date")]
    #[serde(with = "crate::client::rfc3339_opt")]
    pub updated_at: Option<OffsetDateTime>,
    /// Thumbnails.
    #[serde(rename = "work_files")]
    pub thumbnails: PurchasedWorkThumbnails,
    /// Whether the work is downloadable.
    pub downloadable: bool,
}

/// A maker of a purchased work.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedWorkMaker {
    /// Maker ID.
    pub id: String,
    /// Maker name.
    pub name: LocalizedString,
    /// Phonetic of the maker name.
    ///
    /// All characters are in Katakana in Japanese.
    #[serde(deserialize_with = "LocalizedString::deserialize_opt")]
    pub name_phonetic: Option<LocalizedString>,
}

/// Thumbnails of a purchased work.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedWorkThumbnails {
    /// Large size.
    #[serde(rename = "main")]
    pub large: String,
    /// Small size.
    #[serde(rename = "sam")]
    pub small: String,
}
