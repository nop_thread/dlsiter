//! Userbuy API.

mod parse;

use std::fmt;
use std::future::Future;
use std::ops::{self, Bound};

use anyhow::{Context as _, Result};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::client::Client;

use self::parse::UserbuyDocument;

/// Purchased product page.
#[derive(Debug, Clone)]
pub struct PurchasedProductsPage {
    /// Purchased products.
    pub products: Vec<PurchasedProduct>,
    /// Zero-based page index.
    pub page0: u32,
    /// Total pages.
    pub num_pages: u32,
}

impl PurchasedProductsPage {
    /// Returns an iterator of futures of `PurchasedProduct`s.
    pub async fn fetch_pages_stream(
        client: Client,
        skip: u32,
    ) -> Result<impl Iterator<Item = impl Future<Output = Result<Self>>>> {
        const ITEMS_PER_PAGE: u32 = 50;

        let base_query = PurchasedProductsPageQuery {
            // All time.
            duration: Default::default(),
            // Earliest first.
            order: SortOrder::EarliestFirst,
            // Zero-based page index.
            page0: 0,
        };

        let first_page0 = skip / ITEMS_PER_PAGE;
        let first_page_skip = skip % ITEMS_PER_PAGE;

        let num_pages = {
            let uri = base_query.to_string();
            UserbuyDocument::fetch(&client, &uri)
                .await
                .with_context(|| {
                    format!("failed to get the number of pages for query {base_query:?}")
                })?
                .parse_num_pages(&uri)
                .context("failed to parse the number of pages")?
        };
        let num_pages_to_fetch = num_pages - first_page0;
        log::debug!("The number of `userbuy` pages to fetch: {num_pages_to_fetch}");

        Ok((first_page0..num_pages).map(move |page0| {
            let client = client.clone();
            let page1 = page0 + 1;
            let query = PurchasedProductsPageQuery {
                page0,
                ..base_query
            };
            let uri = query.to_string();
            async move {
                log::trace!("Fetching `userbuy` page {page1} of {num_pages}");
                let page_data = UserbuyDocument::fetch(&client, &uri)
                    .await
                    .with_context(|| {
                        format!("failed to fetch `userbuy` page {page1} of {num_pages}")
                    })?;
                log::trace!("successfully fetched `userbuy` page {page1} of {num_pages}");
                let mut products = page_data.parse_items(&uri).with_context(|| {
                    format!("failed to parse `userbuy` page {page1} of {num_pages}")
                })?;
                for (inner_i, product) in products.iter_mut().enumerate() {
                    product.index0 = Some(page0 * ITEMS_PER_PAGE + (inner_i as u32));
                }
                if page0 == first_page0 {
                    let _ = products.drain(..(first_page_skip as usize));
                }

                Ok(Self {
                    products,
                    page0,
                    num_pages,
                })
            }
        }))
    }
}

/// Purchased product.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedProduct {
    /// Index inside the `userbuy` list, if known.
    #[serde(skip)]
    pub index0: Option<u32>,
    /// Datetime the item is bought at.
    ///
    /// This is minutes precision.
    #[serde(with = "time::serde::rfc3339")]
    pub purchased_at: OffsetDateTime,
    /// Thumbnail image URI.
    pub thumbnail_uri: Option<String>,
    /// Title.
    pub title: String,
    /// Product ID.
    ///
    /// # Discontinued and removed products
    ///
    /// Note that discontinued (and removed) products might not have a dedicated
    /// pages nor play pages for the work, and thus no links. In such cases,
    /// the product ID can be retrieved if a thumbnail image is available.
    ///
    /// For example, RJ197197 is no longer sold
    /// (see <https://www.dlsite.com/maniax/work/=/product_id/RJ197197.html>),
    /// but the thumbnail
    /// <https://img.dlsite.jp/resize/images2/work/doujin/RJ198000/RJ197197_img_main_240x240.jpg>
    /// is provided for `userbuy` pages, so this can be used.
    ///
    /// If there are no thumbnails, I don't know how to get the work ID.
    /// I guess it would be impossible...
    pub product_id: Option<String>,
    /// Work IDs.
    ///
    /// # Bundled works
    ///
    /// Note that a single item can contain multiple works.
    /// For example, see <https://www.dlsite.com/books/work/=/product_id/BJ198363.html>
    /// (bundled pack, not sold anymore).
    ///
    /// # Discontinued and removed items
    ///
    /// Note that discontinued (and removed) products might not have a dedicated
    /// pages nor play pages for the work, and thus no links. In such cases,
    /// the product ID can be retrieved if a thumbnail image is available.
    pub works: Vec<WorkInProduct>,
    /// Maker ID.
    pub maker_id: Option<String>,
    /// Maker name.
    pub maker_name: String,
    /// Download page URI.
    ///
    /// Note that this is not the direct link to the materials.
    pub download_page_uri: Option<String>,
    /// Purchase price.
    ///
    /// Note that this can be different from the current regular price.
    pub purchase_price: u32,
}

/// A work info in a purchased product info.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct WorkInProduct {
    /// Work ID.
    pub id: Option<String>,
    /// Work name.
    pub name: String,
}

/// A query for purchased products page.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct PurchasedProductsPageQuery {
    /// Duration.
    duration: YearMonthDuration,
    /// Sort order.
    order: SortOrder,
    /// 0-based page index.
    page0: u32,
}

impl fmt::Display for PurchasedProductsPageQuery {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        /// Base URI for userbuy pages.
        const BASE: &str = "https://www.dlsite.com/home/mypage/userbuy/=";

        // Base URI for purchase pages.
        f.write_str(BASE)?;
        // All media types.
        f.write_str("/type/all")?;
        // Duration.
        match (self.duration.first, self.duration.last) {
            (Some(first), Some(last)) => write!(f, "/start/{first}/start_to_end/1/end/{last}"),
            (Some(first), None) => write!(f, "/start/{first}/start_to_end/1"),
            (None, Some(last)) => write!(f, "/start/{last}/start_to_end/2"),
            (None, None) => f.write_str("/start/all"),
        }?;
        // Sort order.
        let sort_order_code = self.order.to_code();
        write!(f, "/sort/{}/order/{}", sort_order_code, sort_order_code)?;
        // 1-based page index.
        write!(f, "/page/{}", self.page0 + 1)?;

        Ok(())
    }
}

/// Calendar year and month.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct YearMonth {
    /// Year.
    year: u16,
    /// 1-based month.
    month1: u8,
}

impl YearMonth {
    /// Creates a new year and month.
    ///
    /// # Panics
    ///
    /// Panics if the given `month1` is invalid.
    #[allow(dead_code)]
    #[inline]
    pub fn new(year: u16, month1: u8) -> Self {
        if !(1..=12).contains(&month1) {
            panic!("[precondition] `month1` must be a valid month, but got {month1}");
        }
        Self { year, month1 }
    }

    fn next_month(self) -> Self {
        if self.month1 == 12 {
            Self {
                year: self.year + 1,
                month1: 1,
            }
        } else {
            Self {
                month1: self.month1 + 1,
                ..self
            }
        }
    }

    fn prev_month(self) -> Self {
        if self.month1 == 1 {
            Self {
                year: self.year - 1,
                month1: 12,
            }
        } else {
            Self {
                month1: self.month1 - 1,
                ..self
            }
        }
    }
}

impl fmt::Display for YearMonth {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:04}-{:02}", self.year, self.month1)
    }
}

/// Inclusive range of calendar year and month.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub struct YearMonthDuration {
    /// First month (inclusive).
    pub first: Option<YearMonth>,
    /// Last month (inclusive).
    pub last: Option<YearMonth>,
}

impl<T: ops::RangeBounds<YearMonth>> From<T> for YearMonthDuration {
    fn from(range: T) -> Self {
        let first = match range.start_bound() {
            Bound::Included(v) => Some(*v),
            Bound::Excluded(v) => Some(v.next_month()),
            Bound::Unbounded => None,
        };
        let last = match range.end_bound() {
            Bound::Included(v) => Some(*v),
            Bound::Excluded(v) => Some(v.prev_month()),
            Bound::Unbounded => None,
        };
        Self { first, last }
    }
}

/// Sort order.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[non_exhaustive]
enum SortOrder {
    /// Recent first.
    // Currently unused from binary.
    #[allow(dead_code)]
    RecentFirst,
    /// Earliest first.
    EarliestFirst,
}

impl SortOrder {
    /// Returns the corresponding number to use in a URI.
    fn to_code(self) -> u8 {
        match self {
            Self::RecentFirst => 1,
            Self::EarliestFirst => 2,
        }
    }
}
