//! Work count API.

use std::borrow::Cow;
use std::num::NonZeroU32;

use anyhow::{Context as _, Result};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::client::Client;

/// Number of works and limitations for purchases API.
#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub struct WorkCount {
    /// The number of works the user have.
    pub user: u32,
    /// (Unknown.)
    pub production: u32,
    /// The maximum number of the items returned by the purchases API at once.
    pub page_limit: NonZeroU32,
    /// (Unknown.)
    ///
    /// Maybe the maximum number of the concurrent connections for DLsite API?
    pub concurrency: u32,
}

impl WorkCount {
    /// Returns the number of purchases pages.
    #[must_use]
    pub fn num_purchases_pages(&self) -> u32 {
        (self.user + self.page_limit.get() - 1) / self.page_limit.get()
    }
}

impl WorkCount {
    /// Fetches the work count data.
    pub async fn fetch(client: &Client, after: Option<OffsetDateTime>) -> Result<Self> {
        /// Endpoint for the work count.
        const BASE: &str = "https://play.dlsite.com/api/product_count";

        let uri = match after {
            Some(after) => Cow::Owned(format!("{BASE}?last={}", after.unix_timestamp())),
            None => Cow::Borrowed(BASE),
        };

        let resp = client
            .http_client()
            .get(&*uri)
            .send()
            .await
            .and_then(|v| v.error_for_status())
            .context("failed to retrieve work count data")?;
        resp.json::<Self>().await.context("invalid work count data")
    }
}
