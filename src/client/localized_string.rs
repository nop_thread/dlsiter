//! Localized string.

use std::collections::HashMap;

use serde::{Deserialize, Deserializer, Serialize};

/// Localized string.
#[derive(Debug, Clone, Serialize)]
pub struct LocalizedString {
    /// `ja_JP` string.
    #[serde(rename = "ja_JP")]
    ja_jp: Option<String>,
    /// Other languages.
    #[serde(flatten)]
    map: HashMap<String, String>,
}

impl LocalizedString {
    /// Returns the Japanese representation if available.
    #[inline]
    #[must_use]
    pub fn ja_jp(&self) -> Option<&str> {
        self.ja_jp.as_deref()
    }

    /// Returns the US English representation if available.
    #[inline]
    #[must_use]
    pub fn en_us(&self) -> Option<&str> {
        self.get("en_US")
    }

    /// Returns the string in the given language code if available.
    #[must_use]
    pub fn get(&self, lang_code: &str) -> Option<&str> {
        self.map.get(lang_code).map(|s| s.as_str())
    }

    /// Returns some string and its language code.
    ///
    /// `ja_JP` is most preferred, `en_US` second, and if both is missing then
    /// the string in a random language is returned.
    #[must_use]
    pub fn any(&self) -> (&str, &str) {
        self.ja_jp()
            .map(|v| ("ja_JP", v))
            .or_else(|| self.en_us().map(|v| ("en_US", v)))
            .unwrap_or_else(|| {
                let (k, v) = self
                    .map
                    .iter()
                    .next()
                    .expect("the localized string should never be empty");
                (k.as_str(), v.as_str())
            })
    }

    /// Returns some string without language code.
    ///
    /// `ja_JP` is most preferred, `en_US` second, and if both is missing then
    /// the string in a random language is returned.
    #[must_use]
    pub fn any_value(&self) -> &str {
        self.ja_jp().or_else(|| self.en_us()).unwrap_or_else(|| {
            self.map
                .keys()
                .next()
                .expect("the localized string should never be empty")
                .as_str()
        })
    }
}

impl LocalizedString {
    /// Deserializes the optional localized string.
    pub fn deserialize_opt<'de, D>(deserializer: D) -> Result<Option<Self>, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        // Use `Option<String>` as a value, and then normalize them.
        // For example, purchased work item for `RJ282772` contains
        // `"name_phonetic":{"ja_JP":null}`.
        let map = Option::<HashMap<String, Option<String>>>::deserialize(deserializer)?;
        let map = match map {
            Some(v) => v,
            None => return Ok(None),
        };
        let mut map: HashMap<String, String> = map
            .into_iter()
            .filter_map(|(k, v)| v.map(|v| (k, v)))
            .collect();
        if map.is_empty() {
            return Ok(None);
        }
        let ja_jp = map.remove("ja_JP");
        if map.is_empty() {
            // Release allocated memory.
            map = HashMap::new();
        }
        Ok(Some(Self { ja_jp, map }))
    }
}

impl<'de> Deserialize<'de> for LocalizedString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        use serde::de::Error;

        // Use `Option<String>` as a value, and then normalize them.
        // For example, purchased work item for `RJ282772` contains
        // `"name_phonetic":{"ja_JP":null}`.
        let map = HashMap::<String, Option<String>>::deserialize(deserializer)?;
        let mut map: HashMap<String, String> = map
            .into_iter()
            .filter_map(|(k, v)| v.map(|v| (k, v)))
            .collect();
        if map.is_empty() {
            return Err(D::Error::custom("localized string must not be empty"));
        }
        let ja_jp = map.remove("ja_JP");
        if map.is_empty() {
            // Release allocated memory.
            map = HashMap::new();
        }
        Ok(Self { ja_jp, map })
    }
}
