//! Bought products API.

use anyhow::{Context as _, Result};
use serde::{Deserialize, Serialize};

use crate::client::Client;

/// Purchased products.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PurchasedProductList {
    /// Product IDs of the bought items.
    pub boughts: Vec<String>,
    /// Product IDs of the rent items.
    pub rentals: Vec<String>,
    /// (Unknown.)
    pub dlsite_gifts: Vec<String>,
    /// (Unknown.)
    ///
    /// Maybe rated products?
    #[serde(rename = "myrates")]
    pub rated: Vec<String>,
}

impl PurchasedProductList {
    /// Returns the number of products.
    #[inline]
    #[must_use]
    pub fn num_products(&self) -> usize {
        self.boughts.len() + self.rentals.len()
    }
}

impl PurchasedProductList {
    /// Fetches the purchased products list.
    pub async fn fetch(client: &Client) -> Result<Self> {
        /// Endpoint for the purchased product count.
        // Note that this API returns JSON data with `text/html; charset=UTF-8` content-type.
        const URI: &str = "https://www.dlsite.com/home/load/bought/product";

        let resp = client
            .http_client()
            .get(URI)
            .send()
            .await
            .and_then(|v| v.error_for_status())
            .context("failed to retrieve product count data")?;
        resp.json::<Self>()
            .await
            .context("invalid product count data")
    }
}
