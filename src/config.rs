//! Config.

use std::fmt;
use std::fs::File;
use std::io::BufRead as _;
use std::path::Path;

use anyhow::Result;
use serde::{Deserialize, Serialize};

use crate::client::SessionToken;

/// Secrets.
#[derive(Clone, Deserialize, Serialize)]
pub struct Secrets {
    /// Login credentials.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub login_credentials: Option<LoginCredentials>,
    /// Session token.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub session_token: Option<SessionToken>,
}

impl Secrets {
    /// Returns the login credentials.
    #[inline]
    #[must_use]
    pub fn login_credentials(&self) -> Option<&LoginCredentials> {
        self.login_credentials.as_ref()
    }

    /// Returns the session token.
    #[inline]
    #[must_use]
    pub fn session_token(&self) -> Option<&SessionToken> {
        self.session_token.as_ref()
    }
}

impl Secrets {
    /// Loads the secret from the file.
    pub fn load_from_path(path: &Path) -> Result<Option<Self>> {
        match File::open(path) {
            Ok(mut file) => {
                let v = serde_json::from_reader(&mut file)?;
                Ok(Some(v))
            }
            Err(e) => match e.kind() {
                std::io::ErrorKind::NotFound => Ok(None),
                _ => Err(e.into()),
            },
        }
    }

    /// Saves the secret to the file.
    pub fn save_to_path(&self, path: &Path) -> Result<()> {
        let mut file = File::create(path)?;
        serde_json::to_writer(&mut file, self)?;
        Ok(())
    }

    /// Retrieves the login credentials from the interactive console input.
    pub fn from_interactive_console() -> Result<Self> {
        // Get login_id.
        eprint!("DLsite login ID: ");
        let mut login_id = String::new();
        let stdin = std::io::stdin();
        {
            let mut handle = stdin.lock();
            handle.read_line(&mut login_id)?;
        }
        if !login_id.is_empty() {
            assert_eq!(login_id.as_bytes()[login_id.len() - 1], b'\n');
            login_id.truncate(login_id.len() - 1);
        }

        // Get password.
        let password = rpassword::prompt_password("DLsite password: ")?;

        Ok(Self {
            login_credentials: Some(LoginCredentials { login_id, password }),
            session_token: None,
        })
    }
}

/// Login credentials.
#[derive(Clone, Deserialize, Serialize)]
pub struct LoginCredentials {
    /// Login ID.
    login_id: String,
    /// Password.
    password: String,
}

impl LoginCredentials {
    /// Returns the login ID.
    #[inline]
    #[must_use]
    pub(crate) fn login_id(&self) -> &str {
        &self.login_id
    }

    /// Returns the password.
    #[inline]
    #[must_use]
    pub(crate) fn password(&self) -> &str {
        &self.password
    }
}

impl fmt::Debug for LoginCredentials {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("LoginCredentials")
            .field("login_id", &self.login_id)
            .field("password", &"(secret)")
            .finish()
    }
}
