//! CLI options.

pub mod fetch;
pub mod stats;

use std::borrow::Cow;
use std::ops::Bound;
use std::path::{Path, PathBuf};

use anyhow::{anyhow, bail, Context as _, Result};
use clap::{Parser, Subcommand};
use directories::ProjectDirs;
use time::{Duration, OffsetDateTime};

use crate::config::Secrets;
use crate::db::DbPool;

/// CLI arguments and related data.
#[derive(Debug)]
pub struct Cli {
    /// Raw options.
    raw: CliRaw,
    /// Directories.
    dirs: ProjectDirs,
}

impl Cli {
    /// Parses the command line and creates CLI arguments data.
    pub fn parse() -> Result<Self> {
        let raw = CliRaw::parse();
        let dirs = ProjectDirs::from("red", "nops", "dlsiter")
            .ok_or_else(|| anyhow!("failed to get project-specific directory paths"))?;

        Ok(Self { raw, dirs })
    }

    /// Returns the secrets path.
    #[inline]
    #[must_use]
    pub fn secrets_path(&self) -> Cow<'_, Path> {
        self.raw.secrets.as_deref().map_or_else(
            || Cow::Owned(self.dirs.data_dir().join("secrets.json")),
            Cow::Borrowed,
        )
    }

    /// Returns the subcommand.
    #[inline]
    #[must_use]
    pub fn command(&self) -> &Command {
        &self.raw.command
    }

    /// Loads the secret from a file if available.
    pub fn load_secrets(&self) -> Result<Option<Secrets>> {
        if let Some(secrets) = Secrets::load_from_path(&self.secrets_path())? {
            log::debug!("Loaded the secrets from a file");
            return Ok(Some(secrets));
        }
        Ok(None)
    }

    /// Loads the secret if available, or log in using the credentials from the console.
    ///
    /// Returns `(is_retrieved_from_file, secrets)`.
    pub fn load_or_init_secrets(&self) -> Result<(bool, Secrets)> {
        if let Some(secrets) = Secrets::load_from_path(&self.secrets_path())? {
            log::debug!("Loaded the secrets from a file");
            if secrets.session_token().is_some() || secrets.login_credentials.is_some() {
                return Ok((true, secrets));
            }
        }

        let secrets = Secrets::from_interactive_console()?;
        log::debug!("Retrieved the secrets from the user input");
        Ok((false, secrets))
    }

    /// Returns the default database file path.
    pub fn default_database_path(&self) -> PathBuf {
        self.dirs.data_dir().join("data.sqlite3")
    }

    /// Returns the default database file path.
    pub fn database_path<'a>(&self, path: Option<&'a Path>) -> Cow<'a, Path> {
        path.map_or_else(|| Cow::Owned(self.default_database_path()), Cow::Borrowed)
    }

    pub fn connect_or_init_database(&self, path: Option<&Path>) -> Result<DbPool> {
        let path = self.database_path(path);
        DbPool::connect_or_init_database(&path)
    }
}

/// A commandline utility to access DLsite data.
#[derive(Debug, Parser)]
pub struct CliRaw {
    /// File path for secrets.
    #[clap(long)]
    secrets: Option<PathBuf>,
    /// Subcommand.
    #[clap(subcommand)]
    command: Command,
}

/// Subcommand.
#[derive(Debug, Subcommand)]
pub enum Command {
    /// Logs in to DLsite.
    Login {
        /// Saves the login ID and the password to the secrets file.
        // No short options since this will weaken the security.
        #[clap(long, group = "save_id_password")]
        save_id_password: bool,
        /// Remove the login ID and the password from the secrets file.
        #[clap(long, group = "save_id_password")]
        forget_id_password: bool,
        /// Only checks the login state, without attempting to login.
        #[clap(short, long)]
        check: bool,
    },
    /// Logs out from DLsite.
    Logout,
    /// Fetches the purchased works data.
    Fetch {
        /// Database file (SQLite3).
        #[clap(short, long)]
        database: Option<PathBuf>,
        /// Fetch all data.
        #[clap(long)]
        refetch_all: bool,
    },
    /// Prints statistics.
    Stats {
        /// Database file (SQLite3).
        #[clap(short, long)]
        database: Option<PathBuf>,
        /// Stats options.
        #[clap(flatten)]
        opts: StatsOpts,
    },
}

#[derive(Debug, Parser)]
pub struct StatsOpts {
    /// Inclusive start date (UTC).
    #[clap(long, group = "since")]
    pub since_i: Option<String>,
    /// Exclusive start date (UTC).
    #[clap(long, group = "since")]
    pub since_x: Option<String>,
    /// Inclusive end date (UTC).
    #[clap(long, group = "until")]
    pub until_i: Option<String>,
    /// Exclusive end date (UTC).
    #[clap(long, group = "until")]
    pub until_x: Option<String>,
}

impl StatsOpts {
    /// Parses the time.
    fn parse_time(s: &str, now: OffsetDateTime) -> Result<OffsetDateTime> {
        if let Ok(duration) = humantime::parse_duration(s) {
            let duration =
                Duration::try_from(duration).context("unsupported relative time range")?;
            return Ok(now.saturating_sub(duration));
        }
        if let Ok(time_parsed_as_utc) = humantime::parse_rfc3339_weak(s) {
            let since_epoch = time_parsed_as_utc
                .duration_since(std::time::SystemTime::UNIX_EPOCH)
                .context("time before UNIX epoch is not supported")?;
            let since_epoch = Duration::try_from(since_epoch).context("unsupported time")?;
            return Ok(OffsetDateTime::UNIX_EPOCH + since_epoch);
        }

        bail!("failed to parse {s:?} as a datetime");
    }

    /// Returns tghe time range since `since-{i,x}` until `until-{i,x}`.
    pub fn time_range(
        &self,
        now: OffsetDateTime,
    ) -> Result<(Bound<OffsetDateTime>, Bound<OffsetDateTime>)> {
        let since = if let Some(since_i) = &self.since_i {
            let since_i = Self::parse_time(since_i, now)
                .with_context(|| format!("failed to parse `since-i` parameter {since_i:?}"))?;
            Bound::Included(since_i)
        } else if let Some(since_x) = &self.since_x {
            let since_x = Self::parse_time(since_x, now)
                .with_context(|| format!("failed to parse `since-x` parameter {since_x:?}"))?;
            Bound::Excluded(since_x)
        } else {
            Bound::Unbounded
        };
        let until = if let Some(until_i) = &self.until_i {
            let until_i = Self::parse_time(until_i, now)
                .with_context(|| format!("failed to parse `until-i` parameter {until_i:?}"))?;
            Bound::Included(until_i)
        } else if let Some(until_x) = &self.until_x {
            let until_x = Self::parse_time(until_x, now)
                .with_context(|| format!("failed to parse `until-x` parameter {until_x:?}"))?;
            Bound::Excluded(until_x)
        } else {
            Bound::Unbounded
        };

        Ok((since, until))
    }
}
