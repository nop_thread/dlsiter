-- Use this table as a key-value store.
CREATE TABLE dlsiter_state(
	id INTEGER PRIMARY KEY,

	next_userbuy_index INTEGER NOT NULL DEFAULT 0,
	latest_fetched_works_datetime TEXT);
INSERT INTO dlsiter_state(id) VALUES (0);

CREATE TABLE makers(
	maker_internal_id INTEGER PRIMARY KEY,

	maker_id TEXT UNIQUE NOT NULL,
	name TEXT NOT NULL);

CREATE TABLE works(
	work_internal_id INTEGER PRIMARY KEY,

	work_id TEXT UNIQUE,
	name TEXT NOT NULL,
	registered_at TEXT,
	thumbnail_uri_large TEXT,
	thumbnail_uri_small TEXT,
	work_type_internal_id INTEGER,
	maker_internal_id INTEGER NOT NULL,

	FOREIGN KEY (work_type_internal_id) REFERENCES work_types(work_type_internal_id),
	FOREIGN KEY (maker_internal_id) REFERENCES makers(maker_internal_id));

CREATE TABLE products(
	-- Zero-based index in the all userbuy list.
	-- This might be able to be used to precisely determine
	-- when the discontinued product is purchased.
	product_userbuy_index INTEGER PRIMARY KEY,

	-- Product ID can be unknown.
	product_id TEXT UNIQUE,
	name TEXT,
	purchased_at TEXT,
	purchase_datetime_is_precise INTEGER NOT NULL,
	purchase_price INTEGER);

CREATE TABLE works_in_product(
	work_in_product_internal_id INTEGER PRIMARY KEY,

	product_userbuy_index INTEGER NOT NULL,
	work_internal_id INTEGER NOT NULL,
	work_index_in_product INTEGER NOT NULL,

	FOREIGN KEY (product_userbuy_index) REFERENCES products(product_userbuy_index),
	FOREIGN KEY (work_internal_id) REFERENCES works(work_internal_id),
	UNIQUE (product_userbuy_index, work_internal_id),
	UNIQUE (work_internal_id, work_index_in_product));

CREATE TABLE work_types(
	work_type_internal_id INTEGER PRIMARY KEY,

	code TEXT UNIQUE NOT NULL,
	name TEXT UNIQUE);
